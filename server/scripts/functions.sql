create or replace function get_obrigatorias()
  returns table (id_disciplina bigint, codigo_disciplina character(7), nome varchar, ano_grade_obrigatoria date) as $$
  begin
    return query
      select disciplinas.id_disciplina, disciplinas.codigo_disciplina, disciplinas.nome, grade_obrigatoria.ano_grade_obrigatoria
        from disciplinas natural join grade_obrigatoria;
  end
  $$ language plpgsql;

create or replace function get_eletivas()
  returns table (id_disciplina bigint, codigo_disciplina character(7), nome varchar, ano_grade_optativa date, id_modulo integer, id_trilha integer) as $$
  begin
    return query
      select d.id_disciplina, d.codigo_disciplina, d.nome, gop.ano_grade_optativa, m.id_modulo, t.id_trilha
        from
          disciplinas as d,
          grade_optativa as gop,
          optativas_compoem_modulos as ocm,
          modulos as m,
          trilhas as t
        where
          d.id_disciplina = gop.id_disciplina
          and gop.id_disciplina = ocm.id_disciplina
          and gop.ano_grade_optativa = ocm.ano_grade_optativa
          and ocm.id_modulo = m.id_modulo
          and m.id_trilha = t.id_trilha;
  end
  $$ language plpgsql;
